<?php
(function () {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'UniversitaetsbibliothekBraunschweig.UbbsLesesaalsystematik',
        'aufstellung',
        'Lesesaalsystematik der UB Braunschweig'
    );
})();