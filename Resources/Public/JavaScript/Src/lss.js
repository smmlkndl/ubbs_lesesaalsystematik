$(document).ready(function(){
        //Anfangswerte für Lesesaal 1, der per Default angzeigt wird
        //Bei Auswahl eines Lesesaals wird Lesesaalid neu bestimmt und die Anfangswerte geändert
    var lesesaalid = 1;
    $(".ublesesaal-lesesaal-image").hide();
    $("#ublesesaal-lesaal-image-1").show();
    $(".ublesesaal-wrapper").hide();
    $("#ubwrap_1").show();
    $(".ublesesaal-picker-box").click(function(){
        lesesaalid = $(this).attr("id");

        switch(lesesaalid){             //Ändert die Überschrift
                case '1':
                    $("#ublesesaal-lesesaal-titel").text("Lesesaal 1 (1. OG):Ingenieurwissenschaften, Mathematik, Informatik, Architektur");
                    $(".ublesesaal-wrapper").hide();
                    $("#ubwrap_1").show();
                    lesesaalid = 1;
                    break;
                case '2':
                    $("#ublesesaal-lesesaal-titel").text("Lesesaal 2 (2. OG): Natur, Wirtschaft, Recht");
                    $(".ublesesaal-wrapper").hide();
                    $("#ubwrap_2").show();
                    lesesaalid = 2;
                    break;
                case '3':
                    $("#ublesesaal-lesesaal-titel").text("Lesesaal 3 (3. OG): Gesellschaft, Kultur, Sprache");
                    $(".ublesesaal-wrapper").hide();
                    $("#ubwrap_3").show();
                    lesesaalid = 3;
                    break;
                case '4':
                    $("#ublesesaal-lesesaal-titel").text("Erdgeschoss und Freihandmagazin");
                    $(".ublesesaal-wrapper").hide();
                    $("#ubwrap_4").show();
                    lesesaalid = 4;
                    break;
                case '5':
                    $("#ublesesaal-lesesaal-titel").text("Lehrbuchsammlung (Erdgeschoss)");
                    $(".ublesesaal-wrapper").hide();
                    $("#ubwrap_5").show();
                    break;
            }
            
            $(".ublesesaal-lesesaal-image").hide(); //Versteckt und blendet gewünschtes Bild an
            $("#ublesesaal-lesaal-image-"+lesesaalid+"").show();
        });
           
    
    
        
    $("#ublesesaal-lesesaal-titel").text("Lesesaal 1 (1. OG):Ingenieurwissenschaften, Mathematik, Informatik, Architektur ");
    
         //Versteckt alle Lesaalbilder und blendet dann 1 an


    $(".systemstellen").click(function(){
    
    url = "https://systematik.biblio.etc.tu-bs.de/PGLesesaalsystematik/Alles?parameter=tgls"+lesesaalid+""; 
        var fgid = $(this).attr("id");
        urlfg = url+fgid

        if($(this).hasClass("offen")){
           
            $("#teilgebietefuer"+fgid).empty();
            $(".systemstellen").removeClass("offen");
        } else {
           
            $.getJSON(urlfg, function(data){
                var i = 0;
                $.each(data, function(){

                    var teilgebietid = lesesaalid+""+data[i].tgid;

                    if(data[i].tghg == 0){
                        $("#teilgebietefuer"+fgid).append("<p style='font-size:1.25rem;width:95%;text-align:right;font-weight:500;line-height:1.5;padding: 0.9375rem 0  0.9375rem 3.125rem;'><a id='"+teilgebietid+"' href='"+data[i].url_katalog+"' title='Im Katalog anzeigen' target='_blank'>"+data[i].tgkurz+" "+data[i].tglang+"</a></p>");
                    } 
                    else {
                       $("#teilgebietefuer"+fgid).append("<a style='cursor:pointer;width:95%;margin-left:5%;text-align:right;display:block' data-control='' role='tab' aria-controls='collapse' aria-expanded='false' class='accordion__header' id='"+teilgebietid+"' title='Unterstellen anzeigen'>"+data[i].tgkurz+" "+data[i].tglang+"</a><div style='margin-top:1em;width:95%;margin-left:5%;text-align:right'id='hunderterstellen"+teilgebietid+"'></div>");
                        $("#"+teilgebietid).click(function(e){
                            if($(this).hasClass("offen2")){
                                $("#hunderterstellen"+teilgebietid).empty();
                                $(this).removeClass("offen2");
                            }
                            else {
                                e.stopPropagation();
                                var urlhg = "http://systematik.biblio.etc.tu-bs.de:80/PGLesesaalsystematik/Alles?parameter=hgls"+teilgebietid;

                                $.getJSON(urlhg, function(data2){
    
                                  
                                   $.each(data2, function(){
                                 
                                        $("#hunderterstellen"+teilgebietid).append("<p style='font-size:1.25rem;width:90%;text-align:right;font-weight:500;line-height:1.5;padding: 0.9375rem 0 0.9375rem 3.125rem;':><a href='"+this.url_katalog+"' title='Im Katalog anzeigen'>"+this.tbknummer+" "+this.tbkname+"</a></p>");
                                        e.stopPropagation();
                                    });
                                });
                                $(this).addClass("offen2");                                
                            }    
                        });
                    }
                    i=i+1;
                });
            });
            $(this).addClass("offen");
        }
        
    });     




});