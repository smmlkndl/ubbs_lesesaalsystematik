<?php

/**
 * Extension Manager/Repository config file for ext "ubbs_lesesaalsysstematik".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'ubbs lesesaalsystematik',
    'description' => 'Anzeige der Systemstellen nach Lesesälen der Universitätsbibliothek Braunschweig als Typo3-Extension',
    'category' => 'plugin',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-9.5.99',
            'rte_ckeditor' => '8.7.0-9.5.99'
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'UniversitaetsbibliothekBraunschweig\\UbbsLesesaalsystematik\\' => 'Classes'
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Henning Peters',
    'author_email' => 'henning.peters@tu-braunschweig.de',
    'author_company' => 'Universitätsbibliothek Braunschweig',
    'version' => '1.0.0',
];
