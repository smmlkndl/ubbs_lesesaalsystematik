<?php
defined('TYPO3_MODE') || die();

call_user_func(function()
{
    /**
     * Temporary variables
     */
    $extensionKey = 'ubbs_lesesaalsystematik';

    /**
     * Default TypoScript for UbbsLesesaalsystematik
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        $extensionKey,
        'Configuration/TypoScript',
        'ubbs lesesaalsystematik'
    );
});
