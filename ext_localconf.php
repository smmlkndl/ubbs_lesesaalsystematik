<?php
defined('TYPO3_MODE') || die();

/***************
 * Add default RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['ubbs_lesesaalsystematik'] = 'EXT:ubbs_lesesaalsystematik/Configuration/RTE/Default.yaml';

/***************
 * PageTS
 */
 \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TsConfig/Page/All.tsconfig">');
 call_user_func(
	 function()
	 {
	 \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
		'UniversitaetsbibliothekBraunschweig.UbbsLesesaalsystematik',
		'aufstellung',
		[
			  'Aufstellung' => 'list',
 
	   ]
	 );
  });
 