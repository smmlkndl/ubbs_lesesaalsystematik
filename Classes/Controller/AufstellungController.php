<?php
    namespace UniversitaetsbibliothekBraunschweig\UbbsLesesaalsystematik\Controller;
    use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

    class AufstellungController extends ActionController
    {
        public function listAction()
        {
            $json1 = file_get_contents("https://systematik.biblio.etc.tu-bs.de/PGLesesaalsystematik/Alles?parameter=fgls1");
            $obj1 = json_decode($json1, true);
            $this->view->assign("ls1", $obj1); 
            
        
            $json2 = file_get_contents("https://systematik.biblio.etc.tu-bs.de/PGLesesaalsystematik/Alles?parameter=fgls2");
            $obj2 = json_decode($json2, true);
            $this->view->assign("ls2", $obj2);

            $json3 = file_get_contents("https://systematik.biblio.etc.tu-bs.de/PGLesesaalsystematik/Alles?parameter=fgls3");
            $obj3 = json_decode($json3, true);
            $this->view->assign("ls3", $obj3);

            $json4 = file_get_contents("https://systematik.biblio.etc.tu-bs.de/PGLesesaalsystematik/Alles?parameter=fgls4");
            $obj4 = json_decode($json4, true);
            $this->view->assign("ls4", $obj4);
        }

    }
 